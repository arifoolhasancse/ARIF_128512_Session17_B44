<?php

namespace App\Model;

use PDO;
use PDOException;


class Database
{
  public $DBH;

    public function __construct()
    {
        try {
            $this->DBH = new PDO('mysql:host=localhost;dbname=atomic_project_b44', "root", "");
            $this->DBH->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT );

            echo "DB connection SUCCESSFUL!<br>";

        } catch (PDOException $error) {
            print "Error!: " . $error->getMessage() . "<br/>";
            die();
        }
    }
}